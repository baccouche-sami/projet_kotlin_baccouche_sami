# Projet_Kotlin_Baccouche_Sami

Projet Kotlin Baccouche Sami

=> Utilisation API Get NFT (OpenSEA) 

NFT => Les NFT - « non fungible tokens » - sont des jetons numériques uniques, non reproductibles, qui permettent d'authentifier un objet réel ou virtuel. Basés sur la technologie de la blockchain, ils sont notamment utilisés pour identifier des oeuvres d'art numériques qui peuvent ainsi être achetées, vendues ou échangées

Lien de API: https://opensea.io/

==========================================================================================================================================


Models :

Asset: L'objet principal de l'API OpenSea est l' asset , qui représente un élément numérique unique dont la propriété est gérée par la blockchain. Le héros CryptoSaga ci-dessous est un exemple d'actif affiché sur OpenSea.
Lien: https://opensea.io/assets

Collection:Les collections sont des groupes d'articles en vente sur OpenSea. Vous pouvez les acheter tous en une seule transaction, et vous pouvez les créer sans aucune transaction ni essence, tant que vous avez déjà approuvé les actifs à l'intérieur.
Lien: https://opensea.io/rankings

Event: Les événements d'actif représentent les changements d'état qui se produisent pour les actifs. Cela inclut les mettre en vente, enchérir sur eux, les vendre, annuler les ventes, les transférer, etc.


Bundle: Les bundles sont des groupes d'articles en vente sur OpenSea. Vous pouvez les acheter tous en une seule transaction, et vous pouvez les créer sans aucune transaction ni essence, tant que vous avez déjà approuvé les actifs à l'intérieur.

Account: Les comptes représentent les adresses de portefeuille et les noms d'utilisateur associés, si le propriétaire en a saisi un sur OpenSea. 


Les Différents API : 

=> Get All Assets 

=> Get All Collections

=> Get Single Asset

=> Get All Events

=> Get All bundles

=> Get Single Contract





