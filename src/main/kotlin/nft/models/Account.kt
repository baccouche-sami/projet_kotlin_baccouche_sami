package nft.models

data class Account(
    val user: User,
    val address: String,
    val config: String,
    val profileImgUrl: String
)
