package nft.models

import java.util.*

data class AssetContract(
   val address:String,
   val asset_contract_type:String,
   val created_date:Date,
   val name: String,
   val nft_version: String,
   val owner: Account,
   val schema_name:String,
   val symbol:String,
   val description: String,
   val external_link:String,
   val image_url:String,
   val default_to_fiat: Boolean,
   val buyer_fee_basis_points: Int,
   val seller_fee_basis_points: Int
)
