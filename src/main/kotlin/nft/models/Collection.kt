package nft.models

data class Collection(
    val primary_asset_contracts: List<AssetContract>,
    val stats: Stats,
    val name: String,
    val safelist_request_status: String,
)
