package nft.models

data class Stats(
val one_day_volume: Int,
val one_day_change: Int,
val one_day_sales: Int,
val one_day_average_price: Int,
val seven_day_volume: Int,
val seven_day_change: Int,
val seven_day_sales: Int,
val seven_day_average_price: Int,
val thirty_day_volume: Int,
val thirty_day_change: Int,
val thirty_day_sales: Int,
val thirty_day_average_price: Int,
val total_volume: Int,
val total_sales: Int,
val total_supply: Int,
val count: Int,
val num_owners: Int,
val average_price: Int,
val num_reports: Int,
val market_cap: Int,
val floor_price: Int
)
