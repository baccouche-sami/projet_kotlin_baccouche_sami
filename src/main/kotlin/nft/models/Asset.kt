package nft.models
//Asset Objet Principal (NFT Object)
data class Asset(
    val id:Int,
    val token_id:Int,
    val name:String,
    val description:String,
    val num_sales:Int,
    val image_url: String,
    val creator: Account,
    val owner: Account

)
