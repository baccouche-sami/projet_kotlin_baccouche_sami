package nft

import nft.client.NftApiClient

 fun main(args: Array<String>) {
    println("Project NFT API")
    val nftApi = NftApiClient()


    val collection = nftApi.getAllCollectionsList(0,20)
    println("===============================Collections List ======================")
    println(collection.toString())
    //println("${collection::class.simpleName}")



    val asset = nftApi.getSingleAsset("0xb47e3cd837ddf8e4c57f05d70ab865de6e193bbb",1)
    println("===============================Asset Object======================")
    println(asset.toString())



    val assets = nftApi.getAllAssetsList(0,20,"desc")
    println("===============================Assets List ======================")
    println(assets.toString())



    val bundles = nftApi.getAllBundles(0,20)
    println("===============================bundles List ======================")
    println(bundles.toString())


    val events = nftApi.getAllEvents(0,20,false)
    println("===============================Events List ======================")
    println(events.toString())


    val singleContract = nftApi.getSingleContract("0x06012c8cf97bead5deae237070f9587f8e7a266d")
    println("===============================singleContract ======================")
    println(singleContract.toString())


    println("==================================Version 2 API ======================================")
    println("================================== Utilisation des adapteurs pour les valeurs de retour  ======================================")

    //val collection2 = nftApi.getAllCollectionsListv2(0,20)
    println("===============================Collections List ======================")
    //println(collection2.toString())
    //println("${collection::class.simpleName}")



    //val asset2 = nftApi.getSingleAssetv2("0xb47e3cd837ddf8e4c57f05d70ab865de6e193bbb",1)
    println("===============================Asset Object======================")
    //println(asset2.toString())



    //val assets2 = nftApi.getAllAssetsListv2(0,20,"desc")
    println("===============================Assets List ======================")
    //println(assets2.toString())



    //val bundles2 = nftApi.getAllBundlesv2(0,20)
    println("===============================bundles List ======================")
    //println(bundles2.toString())


    //val events2 = nftApi.getAllEventsv2(0,20,false)
    println("===============================Events List ======================")
    //println(events2.toString())


    //val singleContract2 = nftApi.getSingleContractv2("0x06012c8cf97bead5deae237070f9587f8e7a266d")
    println("===============================singleContract ======================")
    //println(singleContract2.toString())

 }