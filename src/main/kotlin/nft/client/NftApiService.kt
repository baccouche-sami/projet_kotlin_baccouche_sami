package nft.client

import com.google.gson.JsonElement
import com.google.gson.internal.LinkedTreeMap
import nft.models.*
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query
import java.lang.reflect.Type

internal interface NftApiService {

    @GET("assets/")
    fun getAllAssetsList(
        @Query("offset") offset: Int,
        @Query("limit") limit: Int,
        @Query("order_direction") order_direction: String
    ): Call<Any>

    @GET("/asset/{asset_contract_address}/{token_id}")
    fun getSingleAsset(
        @Path("asset_contract_address") asset_contract_address: String,
        @Path("token_id") token_id: Int
    ): Call<Any>

    @GET("collections/")
    fun getAllCollectionsList(
        @Query("offset") offset: Int,
        @Query("limit") limit: Int
    ): Call<Any>

    @GET("bundles/")
    fun getAllBundlesList(
        @Query("offset") offset: Int,
        @Query("limit") limit: Int
    ): Call<Any>

    @GET("events/")
    fun getAllEventsList(
        @Query("offset") offset: Int,
        @Query("limit") limit: Int,
        @Query("only_opensea") only_opensea:Boolean
    ): Call<Any>

    @GET("asset_contract/{asset_contract_address}")
    fun getSingleContract(
        @Path("asset_contract_address") asset_contract_address: String
    ): Call<Any>


    @GET("assets/")
    fun getAllAssetsListv2(
        @Query("offset") offset: Int,
        @Query("limit") limit: Int,
        @Query("order_direction") order_direction: String
    ): Call<NamedApiResourceList>

    @GET("/asset/{asset_contract_address}/{token_id}")
    fun getSingleAssetv2(
        @Path("asset_contract_address") asset_contract_address: String,
        @Path("token_id") token_id: Int
    ): Call<NamedApiResource>

    @GET("collections/")
    fun getAllCollectionsListv2(
        @Query("offset") offset: Int,
        @Query("limit") limit: Int
    ): Call<NamedApiResourceList>

    @GET("bundles/")
    fun getAllBundlesListv2(
        @Query("offset") offset: Int,
        @Query("limit") limit: Int
    ): Call<NamedApiResourceList>

    @GET("events/")
    fun getAllEventsListv2(
        @Query("offset") offset: Int,
        @Query("limit") limit: Int,
        @Query("only_opensea") only_opensea:Boolean
    ): Call<NamedApiResourceList>

    @GET("asset_contract/{asset_contract_address}")
    fun getSingleContractv2(
        @Path("asset_contract_address") asset_contract_address: String
    ): Call<NamedApiResource>
}