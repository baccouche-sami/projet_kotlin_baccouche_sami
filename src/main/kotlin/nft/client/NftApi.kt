package nft.client

import com.google.gson.JsonElement
import com.google.gson.internal.LinkedTreeMap
import nft.models.*
import java.lang.reflect.Type

interface NftApi {

    fun getAllAssetsList(offset: Int, limit: Int,order_direction: String): Any

    fun getSingleAsset(asset_contract_address: String, token_id: Int ): Any

    fun getAllCollectionsList(offset: Int, limit: Int): Any

    fun getAllBundles(offset: Int,limit: Int): Any

    fun getAllEvents(offset: Int,limit: Int,only_opensea: Boolean): Any

    fun getSingleContract(asset_contract_address: String): Any

    fun getAllAssetsListv2(offset: Int, limit: Int,order_direction: String): NamedApiResourceList

    fun getSingleAssetv2(asset_contract_address: String, token_id: Int ): NamedApiResource

    fun getAllCollectionsListv2(offset: Int, limit: Int): NamedApiResourceList

    fun getAllBundlesv2(offset: Int,limit: Int): NamedApiResourceList

    fun getAllEventsv2(offset: Int,limit: Int,only_opensea: Boolean): NamedApiResourceList

    fun getSingleContractv2(asset_contract_address: String): NamedApiResource




}