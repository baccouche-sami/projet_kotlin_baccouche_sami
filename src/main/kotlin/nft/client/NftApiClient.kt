package nft.client


import com.google.gson.internal.LinkedTreeMap
import com.google.gson.reflect.TypeToken
import nft.models.ApiResource
import nft.models.NamedApiResource
import nft.models.NamedApiResourceList
import retrofit2.Call
import java.util.*
import kotlin.collections.ArrayList


class NftApiClient(
    ApiConfig: ApiConfig = ApiConfig()
) : NftApi {


    private val service = NftApiServiceImpl(ApiConfig)

    private fun <T> Call<T>.result(): T {
        return execute().let {
            if (it.isSuccessful) it.body()!! else throw ErrorResponse(it.code(), it.message())
        }
    }

    override fun getAllAssetsList(offset: Int, limit: Int, order_direction: String): Any =
         service.getAllAssetsList(offset,limit,order_direction).result()


    override fun getSingleAsset(asset_contract_address: String, token_id: Int): Any {
        //var api = service.getSingleAsset(asset_contract_address,token_id).result()
        //println(api)
        //var res = Asset(api.id,api.token_id,api.name,api.description,api.num_sales,api.image_url,api.creator,api.owner)
        return service.getSingleAsset(asset_contract_address,token_id).result()
    }



    override fun getAllCollectionsList(offset: Int, limit: Int): Any =
        service.getAllCollectionsList(offset,limit).result()

    override fun getAllBundles(offset: Int, limit: Int): Any =
        service.getAllBundlesList(offset, limit).result()

    override fun getAllEvents(offset: Int, limit: Int, only_opensea: Boolean): Any =
        service.getAllEventsList(offset,limit,only_opensea).result()


    override fun getSingleContract(asset_contract_address: String): Any =
        service.getSingleContract(asset_contract_address).result()

    override fun getAllAssetsListv2(offset: Int, limit: Int, order_direction: String): NamedApiResourceList {
        return service.getAllAssetsListv2(offset,limit,order_direction).result()
    }

    override fun getSingleAssetv2(asset_contract_address: String, token_id: Int): NamedApiResource {
        return service.getSingleAssetv2(asset_contract_address,token_id).result()
    }

    override fun getAllCollectionsListv2(offset: Int, limit: Int): NamedApiResourceList {
        return service.getAllCollectionsListv2(offset,limit).result()
    }

    override fun getAllBundlesv2(offset: Int, limit: Int): NamedApiResourceList {
        return service.getAllBundlesListv2(offset, limit).result()
    }

    override fun getAllEventsv2(offset: Int, limit: Int, only_opensea: Boolean): NamedApiResourceList {
        return service.getAllEventsListv2(offset,limit,only_opensea).result()
    }

    override fun getSingleContractv2(asset_contract_address: String): NamedApiResource {
        return service.getSingleContractv2(asset_contract_address).result()
    }


}